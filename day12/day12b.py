from collections import defaultdict


def count(curr, visited=[], second=False):
    # print(curr, visited)
    if curr == "end":
        return 1
    if curr.islower():
        if curr in visited:
            if curr == "start":
                return 0
            if second:
                return 0
            else:
                second = True
        visited = visited + [curr]
    return sum(count(n, visited, second) for n in ns[curr])


with open("day12/input", "r") as f:
    ls = f.read().splitlines()

ns = defaultdict(list)
for l in ls:
    a, b = l.split("-")
    ns[a].append(b)
    ns[b].append(a)

print(count("start"))
