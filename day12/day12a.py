from collections import defaultdict


def count(curr, visited=[]):
    if curr == "end":
        return 1
    if curr.islower():
        if curr in visited:
            return 0
        visited = visited + [curr]
    return sum(count(n, visited) for n in ns[curr])


with open("day12/input", "r") as f:
    ls = f.read().splitlines()

ns = defaultdict(list)
for l in ls:
    a, b = l.split("-")
    ns[a].append(b)
    ns[b].append(a)

print(count("start"))
