from parse import parse

with open('day02/input', 'r') as f:
    lines = f.read().splitlines()

x, y = 0, 0
for l in lines:
    dir, val = parse("{} {:d}", l)
    if dir == "forward":
        x += val
    elif dir == "down":
        y += val
    elif dir == "up":
        y -= val

print(x*y)
