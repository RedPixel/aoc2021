from parse import parse

with open('day02/input', 'r') as f:
    lines = f.read().splitlines()

x, y, aim = 0, 0, 0
for l in lines:
    dir, val = parse("{} {:d}", l)
    if dir == "forward":
        x += val
        y += aim*val
    elif dir == "down":
        aim += val
    elif dir == "up":
        aim -= val
print(x*y)
