import math
from operator import *
from functools import reduce


def eat_b(data, n):
    return data[n:],  data[:n]


def eat(data, n):
    return data[n:], int(data[:n], 2)


def parse(data):
    global vsum

    data, v = eat(data, 3)
    vsum += v
    data, t = eat(data, 3)
    if t == 4:
        lit = ""
        more = True
        while more:
            data, more = eat(data, 1)
            data, b = eat_b(data, 4)
            lit += b
        return data, int(lit, 2)
    else:
        data, i = eat(data, 1)
        values = []
        if i == 0:
            data, l = eat(data, 15)
            subdata = data[:l]
            while len(subdata) > 0:
                subdata, val = parse(subdata)
                values.append(val)
            data = data[l:]
        else:
            data, n = eat(data, 11)
            for _ in range(n):
                data, val = parse(data)
                values.append(val)

    f = [add, mul, min, max, None, gt, lt, eq][t]
    val = reduce(f, values)
    return data, val


with open("day16/input", "r") as f:
    input = f.read().strip()

bits = '{:04b}'.format(int(input, 16))
padlen = 4*len(input) - len(bits)
bits = '0'*padlen + bits
vsum = 0
_, ans = parse(bits)
print(vsum)
print(ans)
