from parse import parse
from collections import defaultdict

with open('day05/input', 'r') as f:
    lines = f.read().splitlines()

c = defaultdict(int)
for l in lines:
    x1, y1, x2, y2 = parse("{:d},{:d} -> {:d},{:d}", l)
    if(x1 == x2):
        for y in range(min(y1, y2), max(y1, y2)+1):
            c[(x1, y)] += 1
    elif(y1 == y2):
        for x in range(min(x1, x2), max(x1, x2)+1):
            c[(x, y1)] += 1
    else:
        xs = range(x1, x2+1) if x1 < x2 else range(x1, x2-1, -1)
        ys = range(y1, y2+1) if y1 < y2 else range(y1, y2-1, -1)
        for x, y in zip(xs, ys):
            c[(x, y)] += 1

print(sum(v > 1 for v in c.values()))
