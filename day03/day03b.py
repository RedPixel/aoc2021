def filter(a, most=True):
    bits = [0]*12
    input = a.copy()
    for i in range(12):
        for l in input:
            if l[i] == "1":
                bits[i] += 1
            else:
                bits[i] -= 1
        filtered = []
        for l in input:
            if most:
                crit = "1" if bits[i] >= 0 else "0"
            else:
                crit = "0" if bits[i] >= 0 else "1"
            if l[i] == crit:
                filtered.append(l)
        if len(filtered) == 1:
            return int(filtered[0], 2)
        input = filtered


with open('day03/input.txt', 'r') as f:
    lines = f.read().splitlines()

print(filter(lines)*filter(lines, False))
