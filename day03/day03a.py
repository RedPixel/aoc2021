with open('day03/input.txt', 'r') as f:
    lines = f.read().splitlines()

bits = [0]*len(lines[0])
for l in lines:
    for i, b in enumerate(l):
        if b == "1":
            bits[i] += 1
        else:
            bits[i] -= 1

gamma = ""
epsilon = ""
for i, b in enumerate(bits):
    if bits[i] > 0:
        gamma += "1"
        epsilon += "0"
    else:
        gamma += "0"
        epsilon += "1"

print(int(gamma, 2)*int(epsilon, 2))
