class board:
    def __init__(self, numbers):
        self.board = [int(n) for n in numbers.split()]
        self.marked = set()

    def mark(self, n):
        if n in self.board:
            self.marked.add(n)

    def is_winning(self):
        for i in range(0, 25, 5):
            if self.marked.issuperset(self.board[i:i+5]):
                return True
        for i in range(5):
            if self.marked.issuperset(self.board[i::5]):
                return True
        return False

    def score(self):
        return sum(set(self.board).difference(self.marked))


with open('day04/input', 'r') as f:
    lines = f.read().split("\n\n")
draws = [int(n) for n in lines[0].split(',')]
boards = [board(data) for data in lines[1:]]

scores = []
for n in draws:
    rm = []
    for b in boards:
        b.mark(n)
        if b.is_winning():
            scores.append(b.score()*n)
            rm.append(b)  # since you cannot remove whilst in the for-loop
    for b in rm:
        boards.remove(b)  # we remove it here

print(scores[0], scores[-1])
