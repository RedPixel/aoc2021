from copy import deepcopy

with open("day25/input", "r") as f:
    grid = f.read().split("\n")
for i, l in enumerate(grid):
    grid[i] = list(grid[i])
w = len(grid[0])
h = len(grid)

moved = True
step = 0
while moved:
    step += 1
    moved = False
    new = deepcopy(grid)
    for y, l in enumerate(grid):
        for x, c in enumerate(l):
            if grid[y][x] == ">":
                if grid[y][(x+1) % w] == ".":
                    new[y][x] = "."
                    new[y][(x+1) % w] = ">"
                    moved = True
                else:
                    new[y][x] = ">"
    grid = new
    new = deepcopy(grid)
    for y, l in enumerate(grid):
        for x, c in enumerate(l):
            if grid[y][x] == "v":
                if grid[(y+1) % h][x] == ".":
                    new[y][x] = "."
                    new[(y+1) % h][x] = "v"
                    moved = True
                else:
                    new[y][x] = "v"
    grid = new


print(step)
