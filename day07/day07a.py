from statistics import median

with open('day07/input', 'r') as f:
    poss = [int(n) for n in f.read().split(',')]

med = int(median(poss))

fuel = 0
for pos in poss:
    fuel += abs(pos - med)

print(fuel)
