from statistics import mean

with open('day07/input', 'r') as f:
    poss = [int(n) for n in f.read().split(',')]

m = int(mean(poss))

fuel = 0
for pos in poss:
    n = abs(pos - m)
    fuel += (n+1)*(n/2)

print(int(fuel))
