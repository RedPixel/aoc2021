ns = [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]


def flash(c):
    if c not in flashed:
        flashed.append(c)
        x, y = c
        for dx, dy in ns:
            if 0 <= x + dx <= 9 and 0 <= y + dy <= 9:
                if o[y + dy][x + dx] == 9:
                    flash((x + dx, y + dy))
                else:
                    o[y + dy][x + dx] += 1


with open("day11/input.txt", "r") as f:
    ls = f.read().splitlines()
o = [[int(v) for v in l] for l in ls]

total, step, synced, flashed = 0, 0, False, []
while not synced:
    step += 1
    for y, l in enumerate(o):
        for x, v in enumerate(l):
            o[y][x] += 1
            if v >= 9:
                flash((x, y))
    total += len(flashed)
    for x, y in flashed:
        o[y][x] = 0
    if step == 100:
        print(total)
    if len(flashed) == 100:
        synced = True
        print(step)
    flashed = []
