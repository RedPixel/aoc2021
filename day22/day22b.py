from parse import parse

with open("day22/input", "r") as f:
    ls = f.read().splitlines()

cuboids = []
for line in ls:
    action, x0, x1, y0, y1, z0, z1 = parse(
        "{} x={:d}..{:d},y={:d}..{:d},z={:d}..{:d}", line)
    x1, y1, z1 = x1+1, y1+1, z1+1  # inclusive
    cuboid = (x0, x1, y0, y1, z0, z1, action == "on")

    next = []
    for a0, a1, b0, b1, c0, c1, on in cuboids:
        if x1 > a0 and x0 < a1 and y1 > b0 and y0 < b1 and z1 > c0 and z0 < c1:
            if a0 < x0:
                next.append((a0, x0, b0, b1, c0, c1, on))
                a0 = x0
            if a1 > x1:
                next.append((x1, a1, b0, b1, c0, c1, on))
                a1 = x1
            if b0 < y0:
                next.append((a0, a1, b0, y0, c0, c1, on))
                b0 = y0
            if b1 > y1:
                next.append((a0, a1, y1, b1, c0, c1, on))
                b1 = y1
            if c0 < z0:
                next.append((a0, a1, b0, b1, c0, z0, on))
                c0 = z0
            if c1 > z1:
                next.append((a0, a1, b0, b1, z1, c1, on))
                c1 = z1
        else:
            next.append((a0, a1, b0, b1, c0, c1, on))
    next.append(cuboid)
    cuboids = next

print(sum((x1-x0)*(y1-y0)*(z1-z0)
      for x0, x1, y0, y1, z0, z1, a in cuboids if a))
