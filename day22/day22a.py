from parse import parse
with open("day22/input", "r") as f:
    ls = f.read().splitlines()

on = set()
for l in ls:
    action, x0, x1, y0, y1, z0, z1 = parse(
        "{} x={:d}..{:d},y={:d}..{:d},z={:d}..{:d}", l)
    x1, y1, z1 = x1+1, y1+1, z1+1  # inclusive
    for x in range(max(-50, x0), min(51, x1)):
        for y in range(max(-50, y0), min(51, y1)):
            for z in range(max(-50, z0), min(51, z1)):
                if action == "on":
                    on.add((x, y, z))
                else:
                    on.discard((x, y, z))
print(len(on))
