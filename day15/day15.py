import math
from queue import PriorityQueue


def dijkstra(grid, scale=1):
    w, h = len(grid[0]), len(grid)
    ns = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    visited = set()
    d = {(x, y): math.inf for x in range(w*scale) for y in range(h*scale)}
    d[(0, 0)] = 0
    pq = PriorityQueue()
    pq.put((0, (0, 0)))
    while not pq.empty():
        (_, (x, y)) = pq.get()
        visited.add((x, y))

        for dx, dy in ns:
            nx, ny = x+dx, y+dy
            if 0 <= nx < w*scale and 0 <= ny < h*scale:
                if (nx, ny) not in visited:
                    weight = int(grid[ny % h][nx % w]) + (nx//w) + (ny//h)
                    weight = (weight-1) % 9 + 1
                    old = d[(nx, ny)]
                    new = d[(x, y)] + weight
                    if new < old:
                        pq.put((new, (nx, ny)))
                        d[(nx, ny)] = new
    return d


with open("day15/input", "r") as f:
    grid = f.read().splitlines()

print(dijkstra(grid, 1)[(99, 99)])
print(dijkstra(grid, 5)[(499, 499)])
