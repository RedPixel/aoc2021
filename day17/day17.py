
def throw(vx, vy):
    x, y = 0, 0
    while vy >= 0 or y >= y1:
        x += vx
        y += vy
        if vx > 0:
            vx -= 1
        vy -= 1
        if x1 <= x <= x2 and y1 <= y <= y2:
            return True
    return False


x1, x2, y1, y2 = 206, 250, -105, -57
# (-y1)*(-y1-1)//2 or:
print(y1*(y1+1)//2)
print(sum(throw(vx, vy) for vx in range(x2+1) for vy in range(y1, -y1+1)))
