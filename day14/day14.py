from collections import Counter

with open("day14/input", "r") as f:
    template, ls = f.read().split("\n\n")

rules = {}
for l in ls.splitlines():
    a, b = l.split(" -> ")
    rules[a] = b

s = Counter(template)
p = Counter([template[i: i + 2] for i in range(len(template) - 1)])

for i in range(40):
    old = p.copy()
    for a, b in rules.items():
        # [a1][a2] -> [a1][b][a2], so n pairs [a1][a2] become n fewer pairs [a1][a2], n more pairs [a1][b] and n more pairs [b][a1], with n more single [b]
        n = old[a]
        p[a] -= n
        a1, a2 = a
        p[a1 + b] += n
        p[b + a2] += n
        s[b] += n
    if i+1 in [10, 40]:
        print(max(s.values()) - min(s.values()))
