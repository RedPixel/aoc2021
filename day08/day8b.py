def intlen(a, b):
    return len(set(a).intersection(set(b)))


def solve(signal, output):
    seg = [""] * 10

    for s in signal:
        if len(s) == 2:
            seg[1] = s
        elif len(s) == 3:
            seg[7] = s
        elif len(s) == 4:
            seg[4] = s
        elif len(s) == 7:
            seg[8] = s

    for s in signal:
        if len(s) == 5:
            if intlen(s, seg[1]) == 2:
                seg[3] = s
            elif intlen(s, seg[4]) == 2:
                seg[2] = s
            else:
                seg[5] = s
        if len(s) == 6:
            if intlen(s, seg[4]) == 4:
                seg[9] = s
            elif intlen(s, seg[1]) == 2:
                seg[0] = s
            else:
                seg[6] = s

    seg = [sorted(s) for s in seg]
    digits = [str(seg.index(sorted(d))) for d in output]
    return int("".join(digits))


with open("day08/input", "r") as f:
    lines = f.read().splitlines()

result = 0
for l in lines:
    input = l.split("|")
    signal = input[0].split()
    output = input[1].split()
    result += solve(signal, output)

print(result)
