with open('day08/input', 'r') as f:
    lines = f.read().splitlines()

easy = []
for l in lines:
    easy += [d for d in l.split("|")[1].split() if len(d) in {2, 3, 4, 7}]

print(len(easy))
