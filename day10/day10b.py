o = {"(": ")", "[": "]", "<": ">", "{": "}"}
s = {")": 1, "]": 2, "}": 3, ">": 4}


def score(l):
    stack = []
    for c in l:
        if c in ["(", "[", "{", "<"]:
            stack.append(o[c])
        else:
            if c != stack.pop():
                return -1
    score = 0
    while stack:
        c = stack.pop()
        score = score * 5 + s[c]
    return score


with open("day10/input", "r") as f:
    ls = f.read().splitlines()
scores = sorted([score(l) for l in ls if score(l) != -1])
print(scores[len(scores) // 2])
