o = {"(": ")", "[": "]", "<": ">", "{": "}"}
s = {")": 3, "]": 57, "}": 1197, ">": 25137}


def score(l):
    stack = []
    for c in l:
        if c in ["(", "[", "{", "<"]:
            stack.append(o[c])
        else:
            if c != stack.pop():
                return s[c]
    return 0


with open("day10/input", "r") as f:
    ls = f.read().splitlines()
print(sum([score(l) for l in ls]))
