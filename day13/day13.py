from parse import *

with open("day13/input", "r") as f:
    dls, fls = f.read().split("\n\n")

dots = set()
for dl in dls.splitlines():
    x, y = parse("{:d},{:d}", dl)
    dots.add((x, y))

for fl in fls.splitlines():
    axis, value = parse("fold along {:l}={:d}", fl)
    newdots = set()
    for x, y in dots:
        if axis == "x":
            newdots.add((min(x, 2 * value - x), y))
        else:
            newdots.add((x, min(y, 2 * value - y)))
    dots = newdots
    print(len(dots))

x_min, y_min = min(dots)
x_max, y_max = max(dots)
for y in range(y_min, y_max + 1):
    for x in range(x_min, x_max + 1):
        print("@", end=" ") if (x, y) in dots else print(" ", end=" ")
    print()
