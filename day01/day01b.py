with open('day01/input', 'r') as f:
    ds = [int(d) for d in f.read().splitlines()]

total = 0
for i in range(1, len(ds)-2):
    if ds[i+2] > ds[i-1]:
        total += 1
print(total)
