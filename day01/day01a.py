with open('day01/input', 'r') as f:
    ds = [int(d) for d in f.read().splitlines()]

total = 0
for i in range(1, len(ds)):
    if ds[i] > ds[i-1]:
        total += 1
print(total)
