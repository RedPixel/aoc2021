from collections import defaultdict

dirs = [(-1, 0), (1, 0), (0, -1), (0, 1)]


def get_risk(x, y):
    for (dx, dy) in dirs:
        if basin[x, y] >= basin[x + dx, y + dy]:
            return 0
    return basin[x, y] + 1


with open("day09/input", "r") as f:
    ls = f.read().splitlines()

h, w = len(ls), len(ls[1])

# convert to defaultdict(int) to prevent boundary checks
basin = defaultdict(
    lambda: 10,
    {(x, y): int(v) for y, l in enumerate(ls) for x, v in enumerate(l)},
)

risk = sum([get_risk(x, y) for x in range(w) for y in range(h)])
print(risk)
