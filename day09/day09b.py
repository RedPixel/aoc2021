from collections import defaultdict
from math import prod

dirs = [(-1, 0), (1, 0), (0, -1), (0, 1)]


def is_low(x, y):
    for (dx, dy) in dirs:
        if basin[x, y] >= basin[x + dx, y + dy]:
            return False
    return True


def get_size(cs):
    for c in cs:
        x, y = c
        for (dx, dy) in dirs:
            n = (x + dx, y + dy)
            if not n in cs and basin[n] < 9:
                cs.append(n)
    return len(cs)


with open("day09/input", "r") as f:
    ls = f.read().splitlines()

h, w = len(ls), len(ls[1])

# convert to defaultdict(int) to prevent boundary checks
basin = defaultdict(
    lambda: 10,
    {(x, y): int(v) for y, l in enumerate(ls) for x, v in enumerate(l)},
)

sizes = [get_size([(x, y)]) for x in range(w) for y in range(h) if is_low(x, y)]

print(prod(sorted(sizes)[-3:]))
