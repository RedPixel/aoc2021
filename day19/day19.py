from collections import Counter


def check(known, new):
    new_known = []
    new_pos = []
    found = False
    for dim in range(3):
        this_vals = [p[dim] for p in known]
        for other_dim in range(3):
            for scale in [-1, 1]:
                transformed = [beacon[other_dim]*scale for beacon in new]
                dists = [b-a for a in this_vals for b in transformed]
                value, count = Counter(dists).most_common(1)[0]
                if count >= 12:
                    found = True
                    new_known.append([v - value for v in transformed])
                    new_pos.append(value)
                    break
        if not found:
            return False
    pos.append(new_pos)
    knowns.append(list(zip(new_known[0], new_known[1], new_known[2])))
    return True


def manhattan(a, b):
    return sum([abs(a[0]-b[0]), abs(a[1]-b[1]), abs(a[2]-b[2])])


with open("day19/input", "r") as f:
    scanners = f.read().split("\n\n")
beacons = [([tuple([int(a) for a in line.split(",")]) for line in scanner.splitlines()[1:]]) for scanner in scanners]

done = set()
knowns = [beacons[0]]
other = beacons[1:]
pos = [(0, 0, 0)]
while knowns:
    known = knowns.pop()
    still_unknown = []
    for new in other:
        found = check(known, new)
        if not found:
            still_unknown.append(new)
    other = still_unknown
    done.update(known)

print(len(done))
print(max(manhattan(a, b) for a in pos for b in pos))
