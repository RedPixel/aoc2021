# def precompute(days):
#     a = [0]*6
#     for i in range(1,6):
#         c = [0]*9
#         c[i] = 1
#         for day in range(days):
#             c[(day+7)%9] += c[day%9]
#         a[i] = sum(c)
#     return a

# print(precompute(80))
# print(precompute(256))

with open('day06/input', 'r') as f:
    fish = [int(n) for n in f.read().split(',')]

c = [0]*9
for n in fish:
    c[n] += 1
a80 = [0, 1401, 1191, 1154, 1034, 950]
a256 = [0, 6206821033, 5617089148, 5217223242, 4726100874, 4368232009]
print(sum([a*b for a, b in zip(c, a80)]))
print(sum([a*b for a, b in zip(c, a256)]))
