with open('day06/input', 'r') as f:
    fish = [int(n) for n in f.read().split(',')]


def solve(days):
    c = [0]*9
    for n in fish:
        c[n] += 1

    for day in range(days):
        c[(day+7) % 9] += c[day % 9]
    return(sum(c))


print(solve(80))
print(solve(256))
