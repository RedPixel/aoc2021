def add_right(sn, right):
    if isinstance(sn, int):
        return sn + right
    else:
        a, b = sn
        return [a, add_right(b, right)]


def add_left(sn, left):
    if isinstance(sn, int):
        return sn + left
    else:
        a, b = sn
        return [add_left(a, left), b]


def try_explode(sn, d=0):
    if isinstance(sn, int):
        return False, 0, sn, 0

    a, b = sn
    if d == 4:
        return True, a, 0, b
    else:
        exploded, left, a, right = try_explode(a, d+1)
        if exploded:
            return True, left, [a, add_left(b, right)], 0
        exploded, left, b, right = try_explode(b, d+1)
        if exploded:
            return True, 0, [add_right(a, left), b], right
        return False, 0, sn, 0


def try_split(sn):
    if isinstance(sn, int):
        if sn >= 10:
            return True, [sn // 2, (sn+1) // 2]
        else:
            return False, sn
    a, b = sn
    splitted, a = try_split(a)
    if splitted:
        return True, [a, b]
    splitted, b = try_split(b)
    return splitted, [a, b]


def reduce(sn):
    exploded = True  # do..while
    while exploded or splitted:
        exploded, _, sn, _ = try_explode(sn)
        if not exploded:
            splitted, sn = try_split(sn)
    return sn


def magnitude(sn):
    if isinstance(sn, int):
        return sn
    else:
        a, b = sn
        return 3*magnitude(a) + 2*magnitude(b)


with open("day18/input", "r") as f:
    ls = [eval(l) for l in f.read().splitlines()]

a, *bs = ls
for b in bs:
    a = reduce([a, b])
print(magnitude(a))

print(max([magnitude(reduce([a, b])) for a in ls for b in ls if a != b]))
