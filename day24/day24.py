# pip3 install z3-solver
from z3 import *
with open("day24/input", "r") as f:
    ls = f.read().splitlines()

o = Optimize()
digits = [BitVec('d'+str(i), 64) for i in range(14)]

for d in digits:
    o.add(1 <= d, d <= 9)

r = {
    'w': BitVecVal(0, 64),
    'x': BitVecVal(0, 64),
    'y': BitVecVal(0, 64),
    'z': BitVecVal(0, 64),
}

curr = 0
for i, l in enumerate(ls):
    words = l.split()
    op = words[0]
    a = words[1]
    if op == 'inp':
        r[a] = digits[curr]
        curr += 1
        continue

    b = words[2]

    if not b.isalpha():
        b = int(b)
    else:
        b = r[b]

    new = BitVec('new'+str(i), 64)
    if op == 'add':
        o.add(new == r[a] + b)
    elif op == 'mul':
        o.add(new == r[a] * b)
    elif op == 'mod':
        o.add(new == r[a] % b)
    elif op == 'div':
        o.add(new == r[a] / b)
    elif op == 'eql':
        o.add(new == If(r[a] == b, BitVecVal(1, 64), BitVecVal(0, 64)))
    r[a] = new
o.add(r['z'] == 0)

# o.maximize(sum((10 ** i) * d for i, d in enumerate(digits)))
o.minimize(sum((10 ** i) * d for i, d in enumerate(digits)))
print(o.check())
m = o.model()
for d in digits:
    print(m[d], end="")
print()
