def get_val(p, x, y):
    s = ""
    for j in range(y-1, y+2):
        for i in range(x-1, x+2):
            # inside border (not on)
            if b_min < i < b_max-1 and b_min < j < b_max-1:
                s += "1" if (i, j) in p else "0"
            else:
                s += "1" if timer % 2 == 1 else "0"
    return (int(s, 2))


with open("day20/input", "r") as f:
    enhance_string, input = f.read().split("\n\n")
ls = input.splitlines()

lookup = {'.': 0, '#': 1}
e = [lookup[c] for c in enhance_string]

p = set([(x, y) for y, l in enumerate(ls)
        for x, v in enumerate(l) if v == '#'])
b_min, b_max = 0, len(ls)

for timer in range(50):
    new_p = set()
    b_min -= 1
    b_max += 1
    for j in range(b_min, b_max):
        for i in range(b_min, b_max):
            if e[get_val(p, i, j)]:
                new_p.add((i, j))
    p = new_p
    if(timer+1 in [2, 50]):
        print(len(p))
