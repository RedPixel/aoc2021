pos = [6, 9]
score = [0, 0]
turn = 0
die = 1
rolls = 0
while max(score) < 1000:
    pos[turn] = (pos[turn] + die + die + 1 + die + 2 - 1) % 10 + 1
    score[turn] += pos[turn]
    die += 3
    turn = (turn+1) % 2
    rolls += 3
print(min(score)*rolls)
