cache = {}
# there are are n universes to get to m places for m: n
lookup = {3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}


def play(pos, score, turn):
    key = (tuple(pos), tuple(score), turn)
    if key in cache:
        return cache.get(key)
    if score[0] >= 21:
        return [1, 0]
    if score[1] >= 21:
        return [0, 1]

    w0, w1 = 0, 0
    for i in lookup:
        newpos = pos[:]
        newpos[turn] = (pos[turn] + i - 1) % 10 + 1
        newscore = score[:]
        newscore[turn] += newpos[turn]
        n0, n1 = play(newpos, newscore, (turn+1) % 2)
        w0 += lookup[i]*n0
        w1 += lookup[i]*n1

    cache[key] = (w0, w1)
    return w0, w1


print(max(play([6, 9], [0, 0], 0)))
